# CASL with React application

➡ [Presentation online](https://daweo93.gitlab.io/casl-example-presentation/)  
➡ [Working example](https://daweo93.gitlab.io/casl-example/)  
➡ [Source code](https://gitlab.com/Daweo93/casl-example)

#
Created with [reaval.js](https://github.com/hakimel/reveal.js)
